<!DOCTYPE html>
<html lang="de">

<!-- 
  # Nächste Aufgabe: Link auf neue HTML-Seite posten, welche die Api-Abfrage erstellt und die Fragen präsentiert.
  #
  #
 -->

<head>
  <title>Quizmaster</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <script src="jquery-3.4.1.min.js"></script>
  <link rel="stylesheet" href="style.css">
</head>

<!-- navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php">Quizmaster</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" href="https://www.quiz.nalvus.de">Home <span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="https://opentdb.com/api_config.php">API</a>
    </div>
  </div>
</nav>

<!-- body -->

<body class="d-flex flex-column">

  <!-- basic div -->
  <div class="row">
    <div class="col"></div>

    <!-- middle div -->
    <div class="col">
      <br>
      <br>
      <div class="container text-center">
        <h1 class="display-1">API Quiz</h1>
        <br>
        <br>
      </div>



      <!-- form -->
      <div class="form-all">
        <form action="index.php">
          <div class="form-group">
            <label for="number">Anzahl der Fragen:</label>
            <input type="number" name="trivia_amount" id="trivia_amount" class="form-control" min="1" max="50" value="10">
          </div>
          <div class="form-group">
            <label for="trivia_category">Kategorie auswählen:</label>
            <select id="trivia_category" name="trivia_category" class="form-control">
              <option value="any">Any Category</option> == $0
            </select>
          </div>
          <div class="form-group">
            <label for="trivia_difficulty">Schwierigkeit auswählen:</label>
            <select id="trivia_difficulty" name="trivia_difficulty" class="form-control">
            <option value="any">Any Difficulty</option>
            <option value="easy">Leicht</option>
            <option value="medium">Mittel</option>
            <option value="hard">Schwer</option>
            </select>
          </div>
          <div class="form-group">
            <label for="trivia_type">Art der Fragen:</label>
            <select id="trivia_type" name="trivia_type" class="form-control">
            <option value="any">Zufällig</option>
            <option value="multiple">Multiple choice</option>
            <option value="boolean">Wahr / Falsch</option>
            </select>
          </div>
          <br>
          <hr class="rounded">
          <br>
          <button type="submit" id="submit-button">Abschicken</button>
        </form>
      </div>
      <!-- form end-->

      <!-- php -->
      <?php

      ?>
      <!-- php end-->

    </div>
    <div class="col"></div>
  </div>


  <!-- basic div end -->

</body>
<!-- body end -->

<script>
  let dropdown = $('#trivia_category');
  dropdown.prop('selectedIndex', 0);

  const url = 'https://opentdb.com/api_category.php';

  $.getJSON(url, function(data) {
    $.each(data, function(key, entry) {
      $.each(data.trivia_categories, function(key, entry) {
        dropdown.append($('<option></option>').attr('value', entry.id).text(entry.name))
      });
    });
  });
</script>


</html>